﻿using System;
using System.Net.Mail;
using System.Configuration;

using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;


using System.Data;
using System.Data.SqlClient;
using System.IO;
using EvoPdf.HtmlToPdfClient;

namespace cimetricsconsoleapp
{ 
    class Program
    {
        static void Main(string[] args)
        {


            if (args.Length > 0)
            {
                foreach (string str in args)
                {
                    if (str.ToUpper().Contains("VALIDATEGRANTS"))
                    {
                        try
                        {
                            CheckforBadGrants();
                        }
                        catch (Exception ex)
                        {
                            Console.Write("Run  Failed - VALIDATEGRANTS - " + ex.Message);
                            string strBody = "<p>Error in CI Metrics Console Application:</p>";

                            strBody = strBody + "<p>" + "Run  Failed - VALIDATEGRANTS - " + ex.Message + "</p>";
                            SendErrorStatus(strBody, "Error in CI Metrics Console Application");
                        }

                    }
                    else if (str.ToUpper().Contains("RUNCLEANUPONLY"))
                    {
                        try
                        {
                            CleanUpTestGrants();
                        }
                        catch (Exception ex)
                        {
                            Console.Write("Run  Failed - RUNCLEANUPONLY - " + ex.Message);
                            string strBody = "<p>Error in CI Metrics Console Application:</p>";

                            strBody = strBody + "<p>" + "Run  Failed - RUNCLEANUPONLY - " + ex.Message + "</p>";
                            SendErrorStatus(strBody, "Error in CI Metrics Console Application");
                        }

                    }
                    else if (str.ToUpper().Contains("GENERATECHANGEDPDFFILES"))
                    {
                        try
                        {
                            GeneratePDFFiles();
                        }
                        catch (Exception ex)
                        {
                            Console.Write("Run  Failed - GENERATEPDFFILES - " + ex.Message);
                            string strBody = "<p>Error in CI Metrics Console Application:</p>";

                            strBody = strBody + "<p>" + "Run  Failed - GENERATEPDFFILES - " + ex.Message + "</p>";
                            SendErrorStatus(strBody, "Error in CI Metrics Console Application");
                        }
                    }
                    else if (str.ToUpper().Contains("GENERATEALLPDFFILES"))
                    {
                        try
                        {
                            WriteBatchFile();
                        }
                        catch (Exception ex)
                        {
                            Console.Write("Run  Failed - GENERATEALLPDFFILES - " + ex.Message);
                            string strBody = "<p>Error in CI Metrics Console Application:</p>";

                            strBody = strBody + "<p>" + "Run  Failed - GENERATEALLPDFFILES - " + ex.Message + "</p>";
                            SendErrorStatus(strBody, "Error in CI Metrics Console Application");
                        }

                    }

                }
            }
            else
            {
                //WriteBatchFile();

            }
        }

        static void WriteBatchFile()
        {

            string selectSQL = "Select greqid,Email from [dbo].[vwGrantRequests]";

            string connString = ConfigurationManager.AppSettings["DefaultConnection"].ToString();
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            SqlDataReader reader;

            string strGReqID = string.Empty;
            string strEmail = string.Empty;

            string strURL = GetGrantMetricURL(connString);
            //string strBatLine = "\"C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome\" --headless --print-to-pdf=\"" + ConfigurationManager.AppSettings["PDFFileDirectory"].ToString() + "\\{REQUESTID}.pdf\" \"" + strURL + "default.aspx?RequestID={REQUESTID}&Email={EMAIL}\"";

            //File.Delete(strFileName);
            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    strGReqID = reader["greqid"].ToString();
                    strEmail = reader["Email"].ToString();
                    string strFileName = ConfigurationManager.AppSettings["PDFFileDirectory"].ToString() + "\\" + strGReqID + ".pdf";
                    File.Delete(strFileName);
                    SavePDFFilefromURL(strGReqID, strEmail, strFileName);
                    //string strNewBatLine = strBatLine.Replace("{REQUESTID}", strGReqID);
                    //strNewBatLine = strNewBatLine.Replace("{EMAIL}", strEmail);
                    //using (System.IO.StreamWriter file =
                    //    new System.IO.StreamWriter(strFileName, true))
                    //{
                    //    file.WriteLine(strNewBatLine);
                    //}
                }
                reader.Close();
            }
            catch (Exception err)
            {
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

            //System.Diagnostics.Process.Start(strFileName);

            //string[] lines = { "\"C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome\" --headless --print-to-pdf=\"c:\\tmp\\23061.pdf\" \"" + strURL + "?RequestID=2306&Email=mmorrow@mealsonwheelscentraltexas.org\"", "\"C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome\" --headless --print-to-pdf=\"c:\\tmp\\23062.pdf\" \"" + strURL + "?RequestID=2306&Email=mmorrow@mealsonwheelscentraltexas.org\"", "\"C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome\" --headless --print-to-pdf=\"c:\\tmp\\23063.pdf\" \"" + strURL + "defaultprint.aspx?RequestID=2306&Email=mmorrow@mealsonwheelscentraltexas.org\"" };
            //System.IO.File.WriteAllLines(@"C:\tmp\CreateGrants.bat", lines);


            //string text = "A class is the most powerful data type in C#. Like a structure, " +
            //               "a class defines the data and behavior of the data type. ";

            //System.IO.File.WriteAllText(@"C:\tmp\WriteText.txt", text);

            //using (System.IO.StreamWriter file =
            //    new System.IO.StreamWriter(@"C:\tmp\WriteLines2.txt"))
            //{
            //    foreach (string line in lines)
            //    {
            //        if (!line.Contains("Second"))
            //        {
            //            file.WriteLine(line);
            //        }
            //    }
            //}


            //using (System.IO.StreamWriter file =
            //    new System.IO.StreamWriter(@"C:\tmp\WriteLines2.txt", true))
            //{
            //    file.WriteLine("Fourth line");
            //}

        }


        static void CheckforBadGrants()
        {
            string strConnectionString = ConfigurationManager.AppSettings["DefaultConnection"].ToString();

            string selectSQL = "SELECT GREQID FROM vwMetricsDeleted";

            SqlConnection conn = new SqlConnection(strConnectionString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            SqlDataReader reader;

            string  strDeletedMetrics = "";

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (string.IsNullOrEmpty(strDeletedMetrics))
                    {
                        strDeletedMetrics = reader["GREQID"].ToString();
                    }
                    else
                    {
                        strDeletedMetrics = strDeletedMetrics + ", " + reader["GREQID"].ToString();
                    }
                }
                reader.Close();
            }
            catch (Exception err)
            {
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

            if (!string.IsNullOrEmpty(strDeletedMetrics))
            {
                string strBody = "<p>ORPHANED METRICS FOUND</p>";

                strBody = strBody + "<p>" + "The CI metrics console application found the following grant requests had no metrics associated: " + strDeletedMetrics + "</p>";
                SendErrorStatus(strBody, "ORPHANED METRICS FOUND", true);
            }

            strConnectionString = ConfigurationManager.AppSettings["DefaultConnection"].ToString();

            selectSQL = "SELECT GREQID FROM vwMetricsZeroedOut";

            conn = new SqlConnection(strConnectionString);
            cmd = new SqlCommand(selectSQL, conn);

            string strZeroedOutMetrics = "";

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (string.IsNullOrEmpty(strZeroedOutMetrics))
                    {
                        strZeroedOutMetrics = reader["GREQID"].ToString();
                    }
                    else
                    {
                        strZeroedOutMetrics = strZeroedOutMetrics + ", " + reader["GREQID"].ToString();
                    }
                }
                reader.Close();
            }
            catch (Exception err)
            {
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

            if (!string.IsNullOrEmpty(strZeroedOutMetrics))
            {
                string strBody = "<p>ZEROED OUT METRICS FOUND</p>";

                strBody = strBody + "<p>" + "The CI metrics console application found the following grant requests had all zero values associated: " + strZeroedOutMetrics + "</p>";
                SendErrorStatus(strBody, "ZEROED OUT METRICS FOUND", true);
            }
        }

        static void CleanUpTestGrants()
        {
            string strConnectionString = ConfigurationManager.AppSettings["DefaultConnection"].ToString();

            SqlConnection conn = new SqlConnection(strConnectionString);
            SqlCommand cmd = new SqlCommand("[dbo].[CleanUpGrantRequest]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.ExecuteScalar();

            }
            catch (Exception err)
            {
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

        }

        static void GeneratePDFFiles()
        {
            string strConnectionString = ConfigurationManager.AppSettings["DefaultConnection"].ToString();

            string selectSQL = "SELECT LastRun,CurrentSQLTIME FROM vwPDFLastRunTime";

            SqlConnection conn = new SqlConnection(strConnectionString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            SqlDataReader reader;

            string strLastRunDateTime = "";

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    strLastRunDateTime = reader["LastRun"].ToString();
                }
                reader.Close();
            }
            catch (Exception err)
            {
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }



            selectSQL = "SELECT GReqID,LastModifiedDate, email,state, projecttitle, organization, programarea  from vwPDFModifiedRecords where LastModifiedDate > '" + strLastRunDateTime + "'";

            conn = new SqlConnection(strConnectionString);
            cmd = new SqlCommand(selectSQL, conn);

            string strGrantState = "";
            string strRequestID = "";
            string strEmail = "";
            string strProjectTitle = "";
            string strOrganization = "";
            string strProgramArea = "";

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    strRequestID = reader["GReqID"].ToString();
                    strGrantState = reader["state"].ToString();
                    strEmail = reader["email"].ToString();
                    strProjectTitle = reader["projecttitle"].ToString();
                    strOrganization = reader["organization"].ToString();
                    strProgramArea = reader["programarea"].ToString();
                    SendEmailfromURL(strRequestID, strEmail, strGrantState, strProjectTitle, strOrganization, strProgramArea);
                }
                reader.Close();
            }
            catch (Exception err)
            {
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

            conn = new SqlConnection(strConnectionString);
            cmd = new SqlCommand("[dbo].[UpdatePDFModifiedTime]", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.ExecuteScalar();

            }
            catch (Exception err)
            {
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }
        }

        static void SavePDFFilefromURL(string strRequestID, string strEmail, string strFilename)
        {
            string strConnectionString = ConfigurationManager.AppSettings["DefaultConnection"].ToString();

            string strURL = GetGrantMetricURL(strConnectionString);

            string strURl = strURL + "default.aspx?RequestID={REQUESTID}&Email={EMAIL}&SDF=1";
            strURl = strURl.Replace("{REQUESTID}", strRequestID);
            strURl = strURl.Replace("{EMAIL}", strEmail);



            try
            {
                HtmlToPdfConverter htmlToPdfConverter = null;
                htmlToPdfConverter = new HtmlToPdfConverter(true, "http://cimetricpdfcreator.cloudapp.net/");

                htmlToPdfConverter.LicenseKey = "khwMHQ4OHQ8KCR0KEw0dDgwTDA8TBAQEBB0N";


                // create memory stream to save PDF
                //MemoryStream pdfStream = new MemoryStream();

                byte[] outPdfBuffer = null;
                outPdfBuffer = htmlToPdfConverter.ConvertUrl(strURl);

                Stream stream = new MemoryStream(outPdfBuffer);

                // reset stream position
                stream.Position = 0;

                using (FileStream file = new FileStream(strFilename, FileMode.Create, System.IO.FileAccess.Write))
                {
                    byte[] bytes = new byte[stream.Length];
                    stream.Read(bytes, 0, (int)stream.Length);
                    file.Write(bytes, 0, bytes.Length);
                    stream.Close();
                }
            }
            catch
            {

            }
        }
            

        static void SendEmailfromURL(string strRequestID, string strEmail, string nGrantState, string strProjectTitle, string strOrganization, string strFocusAreaReadOnly)
        {

            string strConnectionString = ConfigurationManager.AppSettings["DefaultConnection"].ToString();

            string strURL = GetGrantMetricURL(strConnectionString);

            string strURl = strURL + "default.aspx?RequestID={REQUESTID}&Email={EMAIL}&SDF=1";
            strURl = strURl.Replace("{REQUESTID}", strRequestID);
            strURl = strURl.Replace("{EMAIL}", strEmail);



            try
            {
                HtmlToPdfConverter htmlToPdfConverter = null;
                htmlToPdfConverter = new HtmlToPdfConverter(true, "http://cimetricpdfcreator.cloudapp.net/");

                htmlToPdfConverter.LicenseKey = "khwMHQ4OHQ8KCR0KEw0dDgwTDA8TBAQEBB0N";


                // create memory stream to save PDF
                //MemoryStream pdfStream = new MemoryStream();

                byte[] outPdfBuffer = null;
                outPdfBuffer = htmlToPdfConverter.ConvertUrl(strURl);

                Stream stream = new MemoryStream(outPdfBuffer);

                // reset stream position
                stream.Position = 0;

                // create email message
                MailMessage mail = new MailMessage();

                AddEmailAddresses(mail, "MailEmailTo");

                mail.From = new MailAddress("cimetrics@stdavidsfoundation.org");

                if (strConnectionString.ToUpper().Contains("CIMETRICSDEV"))
                {
                    mail.Subject = "FROM DEV -- ";
                }
                else if (strConnectionString.ToUpper().Contains("CIMETRICSSBX"))
                {
                    mail.Subject = "FROM DEV -- ";
                }

                string strBody = "The following grant request has been updated:<font face=\"courier\">";

                switch (nGrantState)
                {
                    case "0":
                        mail.Subject = mail.Subject + "Grant ID: " + strRequestID + " Goals NOT Set";
                        strBody = "The following grant requests goals have NOT been set:<font face=\"courier\">";
                        break;

                    case "1":
                        mail.Subject = mail.Subject + "Grant ID: " + strRequestID + " Goals Set";
                        strBody = "The following grant requests goals have been set:<font face=\"courier\">";
                        break;

                    case "2":
                        mail.Subject = mail.Subject + "Grant ID: " + strRequestID + " Year 1 - 6 Month Status has been submitted";
                        strBody = "The following grant requests Year 1 - 6 Month Status has been submitted:<font face=\"courier\">";
                        break;

                    case "3":
                        mail.Subject = mail.Subject + "Grant ID: " + strRequestID + " Year 1 - 12 Month Status has been submitted";
                        strBody = "The following grant requests Year 1 - 12 Month Status has been submitted:<font face=\"courier\">";
                        break;


                    case "4":
                        mail.Subject = mail.Subject + "Grant ID: " + strRequestID + " Year 2 - 6 Month Status has been submitted";
                        strBody = "The following grant requests Year 2 - 6 Month Status has been submitted:<font face=\"courier\">";
                        break;

                    case "5":
                        mail.Subject = mail.Subject + "Grant ID: " + strRequestID + " Year 2 - 12 Month Status has been submitted";
                        strBody = "The following grant requests Year 2 - 12 Month Status has been submitted:<font face=\"courier\">";
                        break;
                }

                mail.Subject = mail.Subject + " - Organization:  " + strOrganization;

                strBody = strBody + "<table>";

                //Space
                strBody = strBody + "<tr>";
                strBody = strBody + "<td></td>";
                strBody = strBody + "</tr>";
                //Space
                strBody = strBody + "<tr>";
                strBody = strBody + "<td></td>";
                strBody = strBody + "</tr>";


                strBody = strBody + "<tr>";
                strBody = strBody + "<td style='padding-left:50px'><b>Grant Project Title:</b></td>";
                strBody = strBody + "<td>" + strProjectTitle + "</td>";
                strBody = strBody + "</tr>";
                strBody = strBody + "<tr>";
                strBody = strBody + "<td style='padding-left:50px'><b>Grant Organization:</b></td>";
                strBody = strBody + "<td>" + strOrganization + "</td>";
                strBody = strBody + "</tr>";
                strBody = strBody + "<tr>";
                strBody = strBody + "<td style='padding-left:50px'><b>Grant Request Email:</b></td>";
                strBody = strBody + "<td>" + strEmail + "</td>";
                strBody = strBody + "</tr>";
                strBody = strBody + "<tr>";
                strBody = strBody + "<td style='padding-left:50px'><b>Focus Area:</b></td>";
                strBody = strBody + "<td>" + strFocusAreaReadOnly + "</td>";
                strBody = strBody + "</tr>";
                //strBody = strBody + "</tr>";
                strBody = strBody + "<tr>";

                strBody = strBody + "<tr>";
                strBody = strBody + "<td style='padding-left:50px'><b>Metrics Link:</b></td>";
                strBody = strBody + "<td><a href=\"" + strURL + "default.aspx?RequestID=" + strRequestID + "\">Metrics Link</a></td>";
                strBody = strBody + "</tr>";

                strBody = strBody + "</table>";

                mail.Body = strBody;
                mail.Attachments.Add(new Attachment(stream, strRequestID + ".pdf"));

                mail.IsBodyHtml = true;

                SmtpClient client = new SmtpClient();
                client.Host = "smtp.office365.com";
                client.Credentials = new System.Net.NetworkCredential("cimetrics@stdavidsfoundation.org", "A7SwUn@s");
                client.Port = 587;
                client.EnableSsl = true;


                client.Send(mail);
                // close pdf document

            }
            catch (Exception ex)
            {

            }
        }

        static string GetGrantMetricURL(string connString)
        {
            string selectSQL = "SELECT * from vwMetricPath";

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            SqlDataReader reader;

            string strURL = string.Empty;

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    strURL = reader["MetricURL"].ToString();
                }
                reader.Close();
            }
            catch (Exception err)
            {
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }
            return strURL;
        }


        static void SendErrorStatus(string strMessage, string strSubject, bool bHighPriority = false)
        {

            String userName = ConfigurationManager.AppSettings["MailUsername"].ToString();
            String password = ConfigurationManager.AppSettings["MailPassword"].ToString();

            MailMessage mail = new MailMessage();
            MailAddress from = new MailAddress(userName);

            mail.From = from;
            mail.Subject = strSubject;



            mail.IsBodyHtml = true;
            mail.Body = strMessage;
            if (bHighPriority)
                mail.Priority = MailPriority.High;

            AddEmailAddresses(mail, "ErrorEmailAddresses");

            SmtpClient client = new SmtpClient();
            client.Host = "smtp.office365.com";
            client.Credentials = new System.Net.NetworkCredential(userName, password);
            client.Port = 587;
            client.EnableSsl = true;
            client.Send(mail);
        }

        static void AddEmailAddresses(MailMessage mail, string strSection)
        {
            String strSummaryEmailAddresses = ConfigurationManager.AppSettings[strSection].ToString();

            string[] strAddresses = strSummaryEmailAddresses.Split(';');
            foreach (string strTo in strAddresses)
            {
                MailAddress to = new MailAddress(strTo);
                mail.To.Add(to);
            }
        }
    }
}
